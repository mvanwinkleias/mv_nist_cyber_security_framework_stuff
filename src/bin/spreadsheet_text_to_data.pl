#!/usr/bin/perl

use strict;
use warnings;

use IO::File;
use Data::Dumper;
$Data::Dumper::Indent=1;

use JSON;


# print Dumper(process_fh(\*STDIN));
my $json = JSON->new()->allow_nonref();
$json->canonical([1]);

print $json->pretty->encode(process_fh(\*STDIN)),$/;
exit;
sub process_fh
{
	my ($fh) = @_;

	my $data = {};

	my $fid;
	my $cid;
	my $scid;

	my $line;
	IN_LINE: while (defined ($line = <$fh> ) )
	{
		chomp($line);
		$line =~ s/^\s*//g;
		$line =~ s/\s*$//g;
		if ($line =~ m/^\s*$/)
		{
			next IN_LINE;
		}

		if ($line =~ m/^([A-Z]*)\s\((.+)\)$/)
		{
			$fid = $2;
			$data->{$fid}->{name}=$1;
			next IN_LINE;
		}

		if ($line =~ m/^(.*)\s\((.*)\):\s+(.*)/)
		{
			my $name = $1;
			my $cid = (split('\.', $2))[1];
			my $description = $3;

			if (! length ($description) )
			{
				my $next_line = <$fh>;
				$next_line =~ s/^\s*//g;
				$next_line =~ s/\s*$//g;
				$description = $next_line;
			}

			$data->{$fid}->{categories}->{$cid}->{name} = $name;
			$data->{$fid}->{categories}->{$cid}->{description} = $description;
			next IN_LINE;
		}

		if ($line =~ m/([A-Z]+\.[A-Z]+\-\d+)\s*:\s*(.*)/)
		{
			$scid = $1;
			my $description = $2;

			my @parts = split(/\./,$scid);
			my @index_parts = split('-',$parts[1]);

			$data->{$parts[0]}->{categories}->{$index_parts[0]}->{subcategories}->{$index_parts[1]}->{description} = $description;
		}	
		# print $line,$/;

		
	}

	return $data;
}

