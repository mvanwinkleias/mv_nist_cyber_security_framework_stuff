# mv_nist_cyber_security_framework_stuff

NIST Cyber Security Framework Stuff

In an attempt to get the information that's inside of the "Core"
documentation into some sort of data structure, some work was done.

The document was exctracted, the XML was passed through a formatter.
Then the text extracted.  Fortunately, within the spreadsheet itself
the text is "in order", which allows for us to make a data structure
out of it.

From there, I can (potentially) annotate the data, and give
each category and subcategory voicings that read more like they're
documenting something.

Then, from an Organizational perspective I can have one root doc
that has security stuff related to the organization which then
references security documentation associated with services.

Services also get their own documentation.

In this way, the framework serves as a way to organize security
documentation.

## License

copyright (C) 2022 Martin VanWinkle, Institute for Advanced Study

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See 

* http://www.gnu.org/licenses/

## Description

* src/exports - shows the example json output

# Supplemental Documentation

Supplemental documentation for this project can be found here:

* [Supplemental Documentation](./doc/index.md)

# Installation

Ideally stuff should run if you clone the git repo, and install the deps specified
in either "DEBIAN/control" or "RPM/specfile.spec"

Optionally, you can build a package which will install the binaries in

* /opt/IAS/bin/mv-nist-cyber-security-framework-stuff/

# Building a Package

## Requirements

### All Systems

* fakeroot

### Debian

* build-essential

### RHEL based systems

* rpm-build

## Export a specific tag (or just the project directory)

## Supported Systems

### Debian packages

```
  fakeroot make package-deb
```

### RHEL Based Systems

```
fakeroot make package-rpm
```

